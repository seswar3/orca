# ORCA: Outlier detection and Robust Clustering for Attributed graphs

## References
1. Eswar, S., Kannan, R., Vuduc, R., Park H. ORCA: Outlier detection and Robust Clustering for Attributed graphs. J Glob Optim (2021). [doi](https://doi.org/10.1007/s10898-021-01024-z)

