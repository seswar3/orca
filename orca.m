function [ W,H,H_hat,Z1,Z2, errChange ] = orca( A,S,k,alpha,beta,lam1,lam2, maxi )
%this function solves the equation
% min_W>=0,H>=0,H_hat>=0 ||A-WH.T-Z1||_F^2 + alpha * ||S-H_hat*H.T-Z2||_F^2 +
%                           beta * ||H_hat - H||_F^2 
%                           + lam1 * ||Z1||_{1,1} + lam2 * ||Z2||_{1,1}
% A is a sparse matrix of size mxn
% the Z matrix is the outlier matrix.
if (nargin == 7)
    numIterations = 50;
elseif (nargin == 8)
    numIterations = maxi;
else
    error('incorrect number of arguments');
end
   
[m,n]=size(A);
W = rand(m,k);
H = rand(k,n);
H_hat = rand(k,n);
Z1 = zeros(size(A));
Z2 = zeros(size(S));
%S = squareform(pdist(A'));

currentIteration=1;
%prevErr =  norm(D-Z,'fro')+alpha*sum(sqrt(sum(Z.^2)))+beta * norm(H,1);
prevErr = norm(A-W*H, 'fro') + alpha * norm(S - H_hat' * H_hat, 'fro') + beta * norm(H_hat - H, 'fro');
currentErr = prevErr - 1;

errChange=zeros(numIterations,1);
%convergence is when A \approx WH
while(currentIteration < numIterations)
    DA = A - Z1;
    DS = S - Z2;
    %equation 9 in paper https://arxiv.org/pdf/1703.09646.pdf
    temp = nnlsm_blockpivot(H', DA', false);
    W = temp';
    %equation 10 in paper https://arxiv.org/pdf/1703.09646.pdf
    lhs = [sqrt(alpha)*H';sqrt(beta) * eye(k)];
    rhs = [sqrt(alpha)*DS; sqrt(beta)* H];
    temp = nnlsm_blockpivot(lhs, rhs);
    H_hat = temp;
    %equation 11 in paper https://arxiv.org/pdf/1703.09646.pdf
    lhs = [W; sqrt(alpha)* H_hat'; sqrt(beta)*eye(k)];
    rhs = [A; sqrt(alpha)* DS; sqrt(beta)*H_hat];
    temp = nnlsm_blockpivot(lhs, rhs);
    H = temp;
    %Update Z1,Z2
    if (currentIteration>5)
      Z1 = max(A-(W*H)-(lam1/2), 0);
      CH = (S - H_hat'*H);
      Z2 = max(((CH'+CH)/2)-(lam2/(2*alpha)), 0);
    end

    if (currentIteration>1)
        prevErr = currentErr;
    end
    errChange(currentIteration)=prevErr;
    currentErr = norm(A-(W*H)-Z1, 'fro') + alpha * norm(S-(H_hat'*H_hat)-Z2, 'fro') + beta * norm(H_hat-H, 'fro');
    if ((currentErr > prevErr) && (currentIteration > 10))
        break;
    end
    disp ([currentIteration prevErr currentErr]);
    currentIteration = currentIteration + 1;
end
errChange(currentIteration)=currentErr;
end
